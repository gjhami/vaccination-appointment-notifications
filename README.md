# vaccination-appointment-notifications

Uses a headless firefox browser to scrape the IDOH vaccination signup page. When a new group is allowed to sign up, twilio is used to send a text alert to the recipient's phone number. You can use a twilio trial account and send several messages for free without entering payment information.

See the following webpage for installing and using geckodriver in headless mode: https://towardsdatascience.com/data-science-skills-web-scraping-javascript-using-python-97a29738353f

## Disclaimer
I have no affiliation with the Indiana Department of Health or the CDC. I also make no claims that this code will work as intended, because they may update their website in a way which this code does not anticipate.
