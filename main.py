from selenium import webdriver
import selenium.common.exceptions as se
from selenium.webdriver.firefox.options import Options
import time
from twilio.rest import Client
import random


# Get eligible groups
# Accepts a WebDriver
# Returns a list of eligible groups
def get_eligible_groups(driver, url):
    driver.get(url)
    time.sleep(5)
    group_elements = driver.find_elements_by_class_name('mat-radio-button')
    group_names = []

    for group_element in group_elements:
        group_name = group_element.text.split('\n')[0]
        group_names.append(group_name)

    return group_names


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # UPDATE THESE VALUES
    # COVID Vaccine Signup Page Information
    signup_url = 'https://vaccine.coronavirus.in.gov'
    # Twilio Information
    account_sid = ""  # Twilio Account SID
    auth_token = ""  # Twilio Account Auth Token
    twilio_phone = ""  # Twilio Generated Phone Number from which to send the update
    # Contact Information
    recipient_phone = ""  # Number which you would like to receive the text update
    num_alerts = 5  # Number of text alerts to send when the signup page changes
    # Dependency Information
    geckodriver_path = ''  # Path to the geckodriver executable

    # Crate the headless WebDriver to scrape the page
    options = Options()
    options.headless = True
    web_driver = webdriver.Firefox(options=options, executable_path=geckodriver_path)

    # Get the groups eligible at the start of the program
    # Attempt every 1-5 minutes until successful if there is an exception; quitting and re-creating the WebDriver
    # after each failed attempt.
    while True:
        try:
            starting_groups = get_eligible_groups(driver=web_driver, url=signup_url)
        except se.WebDriverException as we:
            print("Failed to get eligible groups due to WebDriver exception\n", we)
            print("\nSleeping and retrying...")
            web_driver.quit()
            web_driver = webdriver.Firefox(options=options, executable_path=geckodriver_path)
            time.sleep(random.randrange(60, 300, 1))
        except Exception as e:
            print("Failed due to other exception\n", e)
            print("\nSleeping and retrying...")
            web_driver.quit()
            web_driver = webdriver.Firefox(options=options, executable_path=geckodriver_path)
            time.sleep(random.randrange(60, 300, 1))
        else:
            break

    # Run a loop every 1-5 minutes
    while True:
        # If the maximum number of alerts has already been sent, then exit the program
        if num_alerts <= 0:
            web_driver.quit()
            break

        time.sleep(random.randrange(60, 300, 1))  # Sleep 1-5 minutes

        # Get an updated list of eligible groups
        # Attempt every 1-5 minutes until successful if there is an exception
        while True:
            try:
                eligible_groups = get_eligible_groups(driver=web_driver, url=signup_url)
            except se.WebDriverException as we:
                print("Failed to get eligible groups due to WebDriver exception\n", we)
                print("\nSleeping and retrying...")
                web_driver.quit()
                web_driver = webdriver.Firefox(options=options, executable_path=geckodriver_path)
                time.sleep(random.randrange(60, 300, 1))
            except Exception as e:
                print("Failed due to other exception\n", e)
                print("\nSleeping and retrying...")
                web_driver.quit()
                web_driver = webdriver.Firefox(options=options, executable_path=geckodriver_path)
                time.sleep(random.randrange(60, 300, 1))
            else:  # If successful, break out of the loop
                break

        # If the original and current lists of eligible groups are not the same
        if not (starting_groups == eligible_groups):

            # Construct an update containing the names of the groups currently eligible
            vaccination_update = "The following groups are currently eligible:\n"
            for eligible_group in eligible_groups:
                vaccination_update += "\t" + eligible_group + "\n"

            print(vaccination_update)  # Print the update

            # Attempt to send a message over text every 1-5 minutes until successful
            while True:
                try:
                    # And text the update using a free twilio trial account
                    client = Client(account_sid, auth_token)
                    client.messages.create(to=recipient_phone, from_=twilio_phone, body=vaccination_update)
                except Exception as e:
                    print("Failed to send update text message:\n", e)
                    print("\nWaiting and retrying...")
                    time.sleep(random.randrange(60, 300, 1))
                else:
                    num_alerts = num_alerts - 1
                    break
